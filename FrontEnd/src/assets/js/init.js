(function($){
  $(function(){

    $('.sidenav').sidenav();
    $('.parallax').parallax();
    $('.modal').modal();
    $('select').formSelect();
    $('.datepicker').datepicker();

  }); // end of document ready
})(jQuery); // end of jQuery name space
