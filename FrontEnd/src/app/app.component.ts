import { Component } from '@angular/core';
import { InfoPaydayService } from './services/info-payday.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'frontendPayday';
  constructor( public infoPaydayService: InfoPaydayService ) {

  }
}
