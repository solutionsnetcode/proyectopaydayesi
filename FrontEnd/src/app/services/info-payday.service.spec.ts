import { TestBed } from '@angular/core/testing';

import { InfoPaydayService } from './info-payday.service';

describe('InfoPaydayService', () => {
  let service: InfoPaydayService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InfoPaydayService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
