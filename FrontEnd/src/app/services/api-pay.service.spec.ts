import { TestBed } from '@angular/core/testing';

import { ApiPayService } from './api-pay.service';

describe('ApiPayService', () => {
  let service: ApiPayService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApiPayService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
