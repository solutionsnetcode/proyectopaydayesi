import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ModifyUser } from '../models/modifyUser';
import { Global } from './global';

@Injectable()
export class ModifyUserService{
    public url: string;

    constructor(
        private http: HttpClient
    ){
        this.url = Global.url;
    }
    testService(){
        return 'Probando Modificar Usuario';
    }
    saveModifyUser(modifyUser: ModifyUser): Observable<any>{
        const body = JSON.stringify(modifyUser);
        const headers = new HttpHeaders().set('Content-Type', 'application/json');

        return this.http.put(this.url + 'user/modify', body, {headers});

    }
}
