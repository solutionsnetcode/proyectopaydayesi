import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// import { InfoApi } from '../interfaces/info-payday.interface';

@Injectable({
  providedIn: 'root'
})
export class ApiPayService {
  // api: InfoApi = {};
  private baseUrl = 'http://127.0.0.1:8000';

  constructor(private http: HttpClient) { }

  login(data) {
    return this.http.get(`${this.baseUrl}/login`, data);
  }

  registro(data) {
    return this.http.post(`${this.baseUrl}/user/registro`, data);
  }

  usuarioId(data) {
    return this.http.get(`${this.baseUrl}/user/{id?}`, data);
  }

  usuarios(data) {
    return this.http.get(`${this.baseUrl}/user`, data);
  }

  modificarUsuario(data) {
    return this.http.put(`${this.baseUrl}/user/modify`, data);
  }

  recuperarPass(data) {
    return this.http.get(`${this.baseUrl}/recuperarContraseña`, data);
  }

  servicios(data: any) {
    return this.http.get(`${this.baseUrl}/servicios`, data);
  }

  metodosPago(data) {
    return this.http.get(`${this.baseUrl}/metodosDePago`, data);
  }

  nuevoPago(data) {
    return this.http.post(`${this.baseUrl}/nuevoPago`, data);
  }

}
