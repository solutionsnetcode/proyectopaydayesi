import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { InfoPagina, InfoServicio } from '../interfaces/info-payday.interface';


@Injectable({
  providedIn: 'root'
})
export class InfoPaydayService {

  info: InfoPagina = {};
  cargada = false;

  servicio: any[] = [];

  listaPago: any[] = [];

  equipo: any[] = [];

  carrusel: any[] = [];

  constructor( private http: HttpClient ) {
    // console.log('Servicio de infoPagina listo');
    // Leer archivo json
    this.cargarInfo();
    this.cargarServicios();
    this.cargarEquipo();
    this.cargarCarrusel();
    this.cargarListaPagos();

  }

  private cargarInfo() {
    this.http.get('assets/data/data-payday.json')
        .subscribe( (resp: InfoPagina) => {
          this.cargada = true;
          this.info = resp;
          // console.log(resp);
        });
  }

  private cargarServicios() {
    this.http.get('http://127.0.0.1:8000/servicios')
        .subscribe( (resp: any[]) => {
          this.servicio = resp;
          // this.servicio = Object.values(resp);
          console.log(resp);
          // console.log(Object.values(resp));
        });
  }

  private cargarListaPagos() {
    this.http.get('http://127.0.0.1:8000/metodosDePago')
        .subscribe( (resp: any[]) => {
          this.listaPago = resp;
          // this.servicio = Object.values(resp);
          console.log(resp);
          // console.log(Object.values(resp));
        });
  }

  private cargarEquipo() {
    this.http.get('https://apifirebase-payday.firebaseio.com/equipo.json')
        .subscribe( (resp: any[]) => {
          this.equipo = resp;
          // console.log(resp);
        });
  }

  private cargarCarrusel() {
    this.http.get('https://apifirebase-payday.firebaseio.com/carrusel.json')
        .subscribe( (resp: any[]) => {
          this.carrusel = resp;
          console.log(resp);
        });
  }
}
