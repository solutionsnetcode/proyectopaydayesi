import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Pago } from '../models/pago';
import { Global } from './global';

@Injectable()
export class PagosService{
    public url: string;

    constructor(
        private http: HttpClient
    ){
        this.url = Global.url;
    }
    testService(){
        return 'Probando Servicio de Pago';
    }
    savePago(pago: Pago): Observable<any>{
        const body = JSON.stringify(pago);
        const headers = new HttpHeaders().set('Content-Type', 'application/json');

        return this.http.post(this.url + 'nuevoPago', body, {headers});
    }
}
