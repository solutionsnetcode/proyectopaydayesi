import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ReenviarPass } from '../models/reenviarPass';
import { Global } from './global';

@Injectable()
export class RePassService{
    public url: string;

    constructor(
        private http: HttpClient
    ){
        this.url = Global.url;
    }
    testService(){
        return 'Probando reenviar pass...';
    }
    saveRePass(repass: ReenviarPass): Observable<any>{
        const body = JSON.stringify(repass);
        const headers = new HttpHeaders().set('Content-Type', 'application/json');

        return this.http.post(this.url + 'recuperarContraseña', body, {headers});

    }
}
