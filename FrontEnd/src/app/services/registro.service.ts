import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Registro } from '../models/registro';
import { Global } from './global';

@Injectable()
export class RegistroService{
    public url: string;

    constructor(
        private http: HttpClient
    ){
        this.url = Global.url;
    }
    testService(){
        return 'Probando Servicio de Registro';
    }
    saveRegistro(registro: Registro): Observable<any>{
        const body = JSON.stringify(registro);
        const headers = new HttpHeaders().set('Content-Type', 'application/json');

        return this.http.post(this.url + 'user/registro', body, {headers});

    }
}

