import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Login } from '../models/login';
import { Global } from './global';
import { isNullOrUndefined } from 'util';

@Injectable()
export class LoginService{
    public url: string;

    constructor(
        private http: HttpClient
    ){
        this.url = Global.url;
    }
    headers: HttpHeaders = new HttpHeaders({
        'Content-Type': 'application/json'
    });

    testService(){
        return 'Probando Login...';
    }
    saveLogin(login: Login): Observable<any>{
        // const body = JSON.stringify(login);
        // const headers = new HttpHeaders().set('Content-Type', 'application/json');

        return this.http.post(this.url + 'login', login);
        // console.log(postMessage);

    }
    setUser(user: Login): void {
        const userString = JSON.stringify(user);
        localStorage.setItem('currentUser', userString);
      }

    setToken(token): void {
        localStorage.setItem('accessToken', token);
    }

    getToken() {
        return localStorage.getItem('accessToken');
    }

    getCurrentUser(){
        // const userString = localStorage.getItem('currentUser');
        // if (`userString === null`) {
        //  const login: Login = JSON.parse(userString);
        //  return login;
        // } else {
        //  return null;
        // }
    }

    logoutUser() {
    //    const accessToken = localStorage.getItem('accessToken');
    //    const urlApi = `http://localhost:4200/logout?access_token=${accessToken}`;
    //    localStorage.removeItem('accessToken');
    //    localStorage.removeItem('currentUser');
    //    return this.http.post<Login>(urlApi, { headers: this.headers });
      }
}
