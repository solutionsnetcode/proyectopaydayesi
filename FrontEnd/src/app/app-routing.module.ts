import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InicioComponent } from './pages/inicio/inicio.component';
import { LoginComponent } from './pages/login/login.component';
import { RegistroComponent } from './pages/registro/registro.component';
import { ReenviarPassComponent } from './pages/reenviar-pass/reenviar-pass.component';
import { CambiarPassComponent } from './pages/cambiar-pass/cambiar-pass.component';
import { NosotrosComponent } from './pages/nosotros/nosotros.component';
import { CatalogoComponent } from './pages/catalogo/catalogo.component';
import { ServiciosComponent } from './pages/servicios/servicios.component';
import { MetodosPagoComponent } from './pages/metodos-pago/metodos-pago.component';
import { ModifyUserComponent } from './pages/modify-user/modify-user.component';


const routes: Routes = [
  {path: 'inicio', component: InicioComponent},
  {path: 'login', component: LoginComponent},
  {path: 'registro', component: RegistroComponent},
  {path: 'reenviarpass', component: ReenviarPassComponent},
  {path: 'cambiarpass', component: CambiarPassComponent},
  {path: 'nosotros', component: NosotrosComponent},
  {path: 'catalogo', component: CatalogoComponent},
  {path: 'servicios', component: ServiciosComponent},
  {path: 'metodos', component: MetodosPagoComponent},
  {path: 'modifyuser', component: ModifyUserComponent},
  {path: '**', pathMatch: 'full', redirectTo: 'inicio'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: false})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
