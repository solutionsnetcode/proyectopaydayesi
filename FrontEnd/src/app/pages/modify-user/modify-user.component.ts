import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ModifyUserService } from '../../services/modifyUser.service';
import { ModifyUser } from '../../models/modifyUser';

@Component({
  selector: 'app-modify-user',
  templateUrl: './modify-user.component.html',
  styleUrls: ['./modify-user.component.css'],
  providers: [ModifyUserService]
})
export class ModifyUserComponent implements OnInit {

  public title: string;
  public modifyUser: ModifyUser;
  public status: string;

  constructor(
    private modifyUserService: ModifyUserService
  ){
    this.title = 'Modificar Usuario';
    this.modifyUser = new ModifyUser('', '', '', '', '', '', '', '', '');
  }

  ngOnInit(): void {
  }

  onSubmit(form) {
    console.log(this.modifyUser);
    this.modifyUserService.saveModifyUser(this.modifyUser).subscribe(
      response => {
        if (response.modifyUser){
          this.status = 'success';
          form.reset();
        }else{
          this.status = 'failed';
        }
      },
      error => {
        console.log(error as any);
      }
    );
  }

}
