import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
// import { ApiPayService } from '../../services/api-pay.service';
import { FormsModule } from '@angular/forms';
import { RegistroService } from '../../services/registro.service';
import { Registro } from '../../models/registro';
import { Location } from '@angular/common';


@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css'],
  providers: [RegistroService]
})
export class RegistroComponent implements OnInit {

  public title: string;
  public registro: Registro;
  public status: string;

  constructor(
    // private ApiPay: ApiPayService,
    // private router: Router
    private registroService: RegistroService, private router: Router
  ){
    this.title = 'Registro de Usuario';
    this.registro = new Registro('', '', '', '', '', '', '');

  }


  ngOnInit(): void {
  }

  onSubmit(form) {
    console.log(this.registro);
    return this.registroService.saveRegistro(this.registro).subscribe(
      response => {
        if (response.registro){
          this.status = 'success';
          this.router.navigate(['/login']);
          // this.location.go());
          form.reset();
        }else{
          this.status = 'failed';
        }
      },
      error => {
        console.log(error as any);
      }
    );
  }

}
