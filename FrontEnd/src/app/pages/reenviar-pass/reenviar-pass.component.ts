import { Component, OnInit } from '@angular/core';
import { RePassService } from '../../services/reeviarPass.service';
import { ReenviarPass } from '../../models/reenviarPass';

@Component({
  selector: 'app-reenviar-pass',
  templateUrl: './reenviar-pass.component.html',
  styleUrls: ['./reenviar-pass.component.css'],
  providers: [RePassService]
})
export class ReenviarPassComponent implements OnInit {

  public title: string;
  public repass: ReenviarPass;
  public status: string;

  constructor(
    private rePassService: RePassService
  ) {
    this.title = 'Solicitar una nueva contraseña';
    this.repass = new ReenviarPass('');
  }

  onSubmit(form) {
    console.log(this.repass);
    this.rePassService.saveRePass(this.repass).subscribe(
      response => {
        if (response.repass){
          this.status = 'success';
          form.reset();
        }else{
          this.status = 'failed';
        }
      },
      error => {
        console.log(error as any);
      }
    );
  }

  ngOnInit(): void {
  }

}

