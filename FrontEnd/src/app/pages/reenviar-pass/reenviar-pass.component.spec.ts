import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReenviarPassComponent } from './reenviar-pass.component';

describe('ReenviarPassComponent', () => {
  let component: ReenviarPassComponent;
  let fixture: ComponentFixture<ReenviarPassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReenviarPassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReenviarPassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
