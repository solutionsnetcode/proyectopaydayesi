import { Component, OnInit } from '@angular/core';

// import { ApiPayService } from '../../services/api-pay.service';
import { InfoPaydayService } from '../../services/info-payday.service';

@Component({
  selector: 'app-servicios',
  templateUrl: './servicios.component.html',
  styleUrls: ['./servicios.component.css']
})
export class ServiciosComponent implements OnInit {

  constructor( public apiPayService: InfoPaydayService ) { }

  ngOnInit(): void {
  }

  onLogout(): void {
    // this.login.logoutUser();
    // location.reload();
  }

}
