import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { PagosService } from 'src/app/services/pagos.service';
import { Pago } from 'src/app/models/pago';


@Component({
  selector: 'app-pagos',
  templateUrl: './pagos.component.html',
  styleUrls: ['./pagos.component.css'],
  providers: [PagosService]
})
export class PagosComponent implements OnInit {

  public title: string;
  public pago: Pago;
  public status: string;

  constructor(
    private pagoService: PagosService
  ){
    this.title = 'Registro de Pagos';
    this.pago = new Pago('', '', '', '', '', '', '');
  }

  ngOnInit(): void {
  }

  onSubmit(form) {
    console.log(this.pago);
    this.pagoService.savePago(this.pago).subscribe(
      response => {
        if (response.pago){
          this.status = 'success';
          form.reset();
        }else{
          this.status = 'failed';
        }
      },
      error => {
        console.log(error as any);
      }
    );
  }

  onLogout(): void {
    // this.login.logoutUser();
    // location.reload();
  }

}
