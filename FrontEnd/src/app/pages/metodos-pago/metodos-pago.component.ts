import { Component, OnInit } from '@angular/core';
import { InfoPaydayService } from '../../services/info-payday.service';

@Component({
  selector: 'app-metodos-pago',
  templateUrl: './metodos-pago.component.html',
  styleUrls: ['./metodos-pago.component.css']
})
export class MetodosPagoComponent implements OnInit {

  constructor( public apiPaySListaPago: InfoPaydayService  ) { }

  ngOnInit(): void {
  }

  onLogout(): void {
    // this.login.logoutUser();
    // location.reload();
  }

}
