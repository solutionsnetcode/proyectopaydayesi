import { Component, OnInit } from '@angular/core';
import { InfoPaydayService } from '../../services/info-payday.service';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {

  constructor( public firebaseCarrusel: InfoPaydayService ) { }

  ngOnInit(): void {
  }

}
