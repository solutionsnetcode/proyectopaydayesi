import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';
import { LoginService } from '../../services/login.service';
import { Login } from '../../models/login';
import { isError } from 'util';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [LoginService]
})
export class LoginComponent implements OnInit {

  // public title: string;
  // public login: Login;
  // public status: string;

  constructor(
    private loginService: LoginService, private router: Router, private location: Location
  ) {
    // this.title = 'Ingresa a tu area privada';
    // this.login = new Login('hugo@gmail.com', 'Hugo12345');
  }

  public login: Login = {
    email: 'hugo@gmail.com',
    password: 'Hugo12345'
  };
  public isError = false;

  ngOnInit(): void { }

  onSubmit(loginForm: NgForm) {
    // console.log(this.login);
    console.log(loginForm.valid);
    if (loginForm.valid) {
      return this.loginService.saveLogin(this.login).subscribe(
        data => {
          this.loginService.setUser(data.user);
          const token = data.id;
          this.loginService.setToken(token);
          this.router.navigate(['/servicios']);
          // location.reload();
          this.isError = false;
        },
        error => this.onIsError()
      );
    }else {
      this.onIsError();
      console.log(Error as any);
    }
  }

  onIsError(): void {
    this.isError = true;
    setTimeout(() => {
      this.isError = false;
    }, 4000);
  }


  // loginu = {
  //  email: 'hugo@gmail.com',
  //  password: 'Hugo12345'
  // };
/*
  public title: string;
  public login: Login;
  public status: string;

  constructor(
    private loginService: LoginService
  ) {
    this.title = 'Ingresa a tu area privada';
    this.login = new Login('hugo@gmail.com', 'Hugo12345');
  }

  onSubmit(loginForm: NgForm) {
    if (loginForm.invalid) {return; }
    console.log(this.login);
    console.log(loginForm.valid);
    return this.loginService.saveLogin(this.login).subscribe(
      response => {
        if (response.login){
          this.status = 'success';
          loginForm.reset();
        }else{
          this.status = 'failed';
        }
      },
      error => {
        console.log(error as any);
      }
    );
  }

  ngOnInit(): void {
  }
*/
}
