import { Component, OnInit } from '@angular/core';
import { InfoPaydayService } from '../../services/info-payday.service';

@Component({
  selector: 'app-nosotros',
  templateUrl: './nosotros.component.html',
  styleUrls: ['./nosotros.component.css']
})
export class NosotrosComponent implements OnInit {

  constructor( public firebaseEquipo: InfoPaydayService ) { }

  ngOnInit(): void {
  }

}
