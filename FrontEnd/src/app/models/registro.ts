export class Registro{
    constructor(
        public nombre: string,
        public apellido: string,
        public documento: string,
        public sexo: string,
        public fechaNacimiento: string,
        public email: string,
        public password: string
    ){}
}
