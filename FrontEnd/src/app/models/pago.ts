export class Pago{
    constructor(
        public monto: string,
        public medioPago: string,
        public moneda: string,
        public servicio: string,
        public usuario: string,
        public numeroTarjeta: string,
        public ccv: string
    ){}
}
