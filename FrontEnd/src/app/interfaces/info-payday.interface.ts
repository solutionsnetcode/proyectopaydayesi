
export interface InfoPagina {
  titulo?: string;
  email?: string;
  equipo_trabajo?: any[];
}

export interface InfoServicio {
  servicios?: Servicio[];
}

export interface Servicio {
  IdServicio?: number;
  Nombre?: string;
  Descripcion?: string;
  FechaRegistro?: string;
  Activo?: number;
  created_at?: any;
  updated_at?: any;
}

