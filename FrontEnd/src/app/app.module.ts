import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './shared/header/header.component';
import { FooterComponent } from './shared/footer/footer.component';
import { InicioComponent } from './pages/inicio/inicio.component';
import { LoginComponent } from './pages/login/login.component';
import { RegistroComponent } from './pages/registro/registro.component';
import { NosotrosComponent } from './pages/nosotros/nosotros.component';
import { CatalogoComponent } from './pages/catalogo/catalogo.component';
import { ReenviarPassComponent } from './pages/reenviar-pass/reenviar-pass.component';
import { CambiarPassComponent } from './pages/cambiar-pass/cambiar-pass.component';
import { ServiciosComponent } from './pages/servicios/servicios.component';
import { MetodosPagoComponent } from './pages/metodos-pago/metodos-pago.component';
import { PagosComponent } from './pages/pagos/pagos.component';
import { ModifyUserComponent } from './pages/modify-user/modify-user.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    InicioComponent,
    LoginComponent,
    RegistroComponent,
    NosotrosComponent,
    CatalogoComponent,
    ReenviarPassComponent,
    CambiarPassComponent,
    ServiciosComponent,
    MetodosPagoComponent,
    PagosComponent,
    ModifyUserComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [HttpClientModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
