import { Component, OnInit } from '@angular/core';
import { InfoPaydayService } from '../../services/info-payday.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  anio: number = new Date().getFullYear();

  constructor( public infoPaydayService: InfoPaydayService ) { }

  ngOnInit(): void {
  }

}
