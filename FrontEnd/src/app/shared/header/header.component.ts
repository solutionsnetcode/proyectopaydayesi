import { Component, OnInit } from '@angular/core';
// import { Login } from '../../models/login';
import { LoginService } from '../../services/login.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor() { }
  public isLogged = false;

  ngOnInit() {
    this.onCheckUser();
  }

  onLogout(): void {
    // this.login.logoutUser();
    // location.reload();
  }

  onCheckUser() {

  }
}
