<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaMonedas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        ////////////////////////////////////////////////////////////
        //       TABLA QUE CONTIENE LAS MONEDAS DEL SISTEMA       //
        ////////////////////////////////////////////////////////////
        Schema::create('Monedas', function (Blueprint $table) {
            $table->tinyIncrements("IdMoneda");
            $table->string("Nombre");
            $table->string("Simbolo");
            $table->boolean("Activo");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Monedas');
    }
}
