<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaCanjeuSuariosProductos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CanjeUsuariosProductos', function (Blueprint $table) {
            $table->increments("IdCanje");
            $table->unsignedInteger("IdUsuario");
            $table->unsignedInteger("IdProductoCatalogo");
            $table->date("FechaRealizado");
            $table->boolean("Anulado")->default(0);
            $table->timestamps();

            ///////////////////
            //CLAVES FORANEAS//
            ///////////////////
            $table->foreign('IdUsuario', 'FK_Usuario_CanjeProducto')->references('IdUsuario')->on('Usuarios')->onDelete('restrict');
            $table->foreign('IdProductoCatalogo', 'FK_Producto_CanjeProducto')->references('IdProductoCatalogo')->on('ProductosCatalogo')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('CanjeUsuariosProductos');
    }
}
