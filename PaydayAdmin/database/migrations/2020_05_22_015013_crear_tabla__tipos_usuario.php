<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaTiposUsuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        ///////////////////////////////////////////////////////////////////////////////////////////////
        // TABLA QUE CONTIENE LOS TIPOS DE USUARIOS DEL SISTEMA (Persona fisica, juridica, empleado) //
        ///////////////////////////////////////////////////////////////////////////////////////////////
        Schema::create('TiposUsuario', function (Blueprint $table) {
            $table->tinyIncrements("Id");
            $table->string("TipoUsuario",30);
            $table->boolean("Activo");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TiposUsuario');
    }
}
