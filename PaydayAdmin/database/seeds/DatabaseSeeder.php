<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(TablaMonedasSeeder::class);//----------------------> Monedas del Sistema
        $this->call(TablaTiposUsuariosSeeder::class);//----------------------> Tipos de Usuarios del Sistema
        $this->call(TablaRolesSeeder::class);//----------------------> Roles del Sistema
        $this->call(TablaPermisosSeeder::class);//----------------------> Permisos del Sistema
        $this->call(TablaUsuarioEmpleadoSeeder::class);//----------------------> Monedas del Sistema
    }
}
