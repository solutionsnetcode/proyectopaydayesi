<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TablaRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = array(
            "1" => array(
                'rol' => 'Marketing',
                'activo' => 1,
            ),
            "2" => array(
                'rol' => 'Operario',
                'activo' => 1,
            ),
            "3" => array(
                'rol' => 'Moderador',
                'activo' => 1,
            ),
            "4" => array(
                'rol' => 'Administrador',
                'activo' => 1,
            )
        );

        foreach ($roles as $key => $rol) {
            DB::table('Roles')->insert([
             'Rol' => $rol['rol'],
             'Activo' => $rol['activo'],
             'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
             'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        } 
    }
}
