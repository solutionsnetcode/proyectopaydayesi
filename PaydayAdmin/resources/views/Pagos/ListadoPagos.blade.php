@extends('layouts/layout')

@section('menu-mediosDePago')
    active
@endsection

@section('styles')

  <style>
    .badge:hover{
      cursor: pointer; 
    }

    .link{
      cursor:pointer;
    }
  </style>

	<link href="{{asset("assets/$AdminPanel/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css") }}" rel="stylesheet" />
	<link href="{{asset("assets/$AdminPanel/plugins/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css") }}" rel="stylesheet" />
	<link href="{{asset("assets/$AdminPanel/plugins/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css") }}" rel="stylesheet" />
  <link href="{{ asset("assets/$AdminPanel/plugins/switchery/switchery.min.css") }}" rel="stylesheet" />
@endsection


@section('contenido')

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1>Listado de Pagos</h1>
        </div>
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Metodos de Pago</a></li>
            </ol>
        </div>
    </div>
</div><!-- /.container-fluid -->
</section>



  <div class="row">
    <div class="panel panel-primary w-100">
      <div class="panel-heading">
        <h4 class="panel-title">Listado</h4>
        <div class="panel-heading-btn">
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-reload" onclick="RecargarClientes();"><i class="fa fa-redo"></i></a>
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
        </div>
      </div>
      <br>
      <div class="panel-body table-responsive">
        <table id="metodosTable" class="table table-bordered table-hover">
          <thead>
            <tr class="text-center">
                <th>N° Pago</th>
                <th>Usuario</th>
                <th>Monto Pago</th>
                <th>Servicio</th>
                <th>Medio de Pago</th> 
                <th>Estado</th>
                <th>Anular</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($pagos as $pago5)
            <tr>
                <td>
               
                </td>
            </tr>

            @endforeach

          </tbody>
        </table>
      </div>
    </div>
       <!-- MODAL -->
   <div class="modal inmodal" style="z-index: 1100;" id="mdlMetodoDePago" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content animated bounceInRight">
            <div class="modal-header" style="margin-top: -20px;">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>          
              <br/>   
              <h4 id="tituloModalEditar" class="modal-title text-center">Nuevo Metodo de Pago</h4>
              <small class="font-bold col-12">No olvide guardar para aplicar los cambios.</small>
            </div>
            <div class="modal-body">
              <div class="row mt-2">
                <input id="txtIdMetodoPago" type="text" value="0" hidden>
                <div class="col-12 mb-4">
                  <label for="txtMetodoDePago" id="lblNombre">Metodo de Pago</label>
                  <input type="text" class="form-control" value="" id="txtMetodoDePago" required> 
                </div>

                <div class="col-12 mb-4">
                  <label for="txtDescripcion" id="lblNombre">Descripcion</label>
                  <textarea id="txtDescripcion" cols="30" class="form-control" rows="4"></textarea>
                </div>

                <div class="col-12 mb-4">
                    <label for="txtUrl" id="lblNombre">UrlPago</label>
                    <input type="text" class="form-control" value="" id="txtUrlPago" required> 
                </div>

                <div class="col-12 mb-4" id="divAltaBaja">
                  <label for="chk">Activo</label><br>
                  <input class="chk ml-4" id="chk" type="checkbox" data-render="switchery" data-theme="blue" checked/> 
                </div>
                
              </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                <button id="btnAltaMetodoPago" type="button" class="btn btn-green">Cargar Metodo de Pago</button>
            </div>
        </div>
    </div>
</div>
  </div>



@endsection

@section('scripts')

<script src="{{asset("assets/$AdminPanel/plugins/datatables.net/js/jquery.dataTables.min.js") }}"></script>
<script src="{{asset("assets/$AdminPanel/plugins/datatables.net-bs4/js/dataTables.bootstrap4.min.js") }}"></script>
<script src="{{asset("assets/$AdminPanel/plugins/datatables.net-responsive/js/dataTables.responsive.min.js") }}"></script>
<script src="{{asset("assets/$AdminPanel/plugins/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js") }}"></script>
<script src="{{asset("assets/$AdminPanel/plugins/datatables.net-buttons/js/dataTables.buttons.min.js") }}"></script>
<script src="{{asset("assets/$AdminPanel/plugins/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js") }}"></script>
<script src="{{asset("assets/$AdminPanel/plugins/datatables.net-buttons/js/buttons.colVis.min.js") }}"></script>
<script src="{{asset("assets/$AdminPanel/plugins/datatables.net-buttons/js/buttons.flash.min.js") }}"></script>
<script src="{{asset("assets/$AdminPanel/plugins/datatables.net-buttons/js/buttons.html5.min.js") }}"></script>
<script src="{{asset("assets/$AdminPanel/plugins/datatables.net-buttons/js/buttons.print.min.js") }}"></script>
<script src="{{asset("js/MedioPago/MediosDePago.js")}}"></script>
<script src="{{ asset("assets/$AdminPanel/plugins/switchery/switchery.min.js") }}"></script>
<script src="{{ asset("assets/$AdminPanel/js/demo/form-slider-switcher.demo.js") }}"></script>

<script>

  

    $(document).ready(function() {    
  
      $('#metodosTable').DataTable({
        pageLength: 25,
        lengthMenu: [[25, 50, 100, -1], [25, 50, 100, 'Todos']],
      //para cambiar el lenguaje a español
  
          "language": {
  
                  "lengthMenu": "Mostrar _MENU_ registros",
                  "zeroRecords": "No se encontraron resultados",  
                  "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", 
                  "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", 
                  "infoFiltered": "(filtrado de un total de _MAX_ registros)", 
                  "sSearch": "Buscar:",
                  "oPaginate": {
                      "sFirst": "Primero",
                      "sLast":"Último",
                      "sNext":"Siguiente",
                      "sPrevious": "Anterior" 
                   },
                   "sProcessing":"Procesando...",
  
              },
              "columnDefs": [
                { className: "text-center align-middle", "targets": [0,1,2,3] },
              ]
  
      });     
  
  });
  
 
  
  </script>

@endsection