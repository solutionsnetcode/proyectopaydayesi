@extends('layouts/layout')

@section('menu-dashboard')
    active
@endsection


@section('contenido')

    <!-- Migas de pan -->
    <ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="javascript:;">Panel de Administración</a></li>
        <li class="breadcrumb-item active">Dashboard</li>
    </ol>
    <!-- /Migas de pan-->

    @csrf
    <!-- Titulo Pagina -->
    <h1 class="page-header">Dashboard</h1>
    <!-- /Titulo Pagina -->

    <div class="row">
        
    </div>

@endsection