@extends('layouts/layout')

@section('menu-permisos')

  active

@endsection


@section('styles')

    <link href="{{ asset("assets/$AdminPanel/plugins/switchery/switchery.min.css") }}" rel="stylesheet" />

@endsection





@section('contenido')
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Gestion de permisos</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Permisos</a></li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <div class="row">
    <div class="panel panel-primary w-100">
      <div class="panel-heading">
        <h4 class="panel-title"></h4>
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
        </div>
      </div>
      <div class="panel-body">
        <div class="row"> 
            <h2 class="col-12 text-center">Rol</h2>
            <select id="cmbRoles" class="col-12 col-xs-8 col-md-5 col-lg-3 form-control m-auto text-center">
                <option value="0" disabled selected="selected">Seleccione un Rol...</option>
                @foreach ($roles as $rol)
                    <option value="{{$rol->IdRol}}">{{$rol->Rol}}</option>
                @endforeach

            </select>

        </div>

        <hr>

        <h2 class="col-12 text-center mt-3 mb-3">Permisos</h2>

        @foreach ($permisos as $permiso)
            <div class="row">
                <table class="col-12 table-hover mb-5">
                        <tr class="col-12 mb-2">
                            <td class="col-10 col-sm-6 col-md-5 col-lg-4 pl-md-5">
                                <span class="negrita">{{$permiso->Permiso}}</span>                    
                            </td>
                            <td class="col-2 col-sm-6 col-md-7 col-lg-8">
                                <input value="{{$permiso->IdPermiso}}" class="chk" id="chk{{$permiso->IdPermiso}}" type="checkbox" data-render="switchery" data-theme="blue"/>
                            </td>

                        </tr>
                </table>               
            </div>
        @endforeach

        <button id="btnActualizarP" class="btn btn-green float-right mb-2 col-12">Actualizar Permisos</button>
      </div>
    </div>
  </div>

    



@endsection





@section('scripts')

    <script src="{{ asset("assets/$AdminPanel/plugins/switchery/switchery.min.js") }}"></script>

    <script src="{{ asset("assets/$AdminPanel/js/demo/form-slider-switcher.demo.js") }}"></script>

    <script type="text/javascript" src="{{asset("js/Permisos/Permisos.js")}}"></script>

@endsection  