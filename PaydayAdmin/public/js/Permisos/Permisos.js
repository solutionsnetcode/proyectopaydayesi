$(document).ready(function () {
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#btnActualizarP').click(actualizarPermisos);
    $("#cmbRoles").change(cargarPermisos);

});// FIN READY

function actualizarPermisos(){
    if($("#cmbRoles").val() <= 0){
        alertaCajaHTML(tipoAlertaError, 'No selecciono Rol', 'Debe seleccionar un Rol para actualizar sus permisos', 'center', animacionEntradaEstirar,animacionSalidaCarreraRapida)
    }
    else{
        $("#mantaLoading").modal('show');
        var datos = new Array();
        $("input").each(function(index, value){
            if(value.id != ''){
                var check = $("#" +  value.id);
                if(check.prop('checked')){
                    datos.push(check.val());
                }
            }        
        });

        $.ajax({
            url:'actualizarPermisos',
            data:{'permisos': datos, 'rol': $("#cmbRoles").val()},
            type:'post',
            dataType: "json",
            success: function (response) {           
                $("#mantaLoading").modal('hide');
                alertaToast(tipoAlertaOK, response.respuesta, 3500);
            },
            statusCode: {
                404: function() {
                alert('El servicio Remextiven no se encuentra disponible');
                }
            },
            error:function(x,xs,xt){
                //nos dara el error si es que hay alguno
                window.open(JSON.stringify(x));
                //alert('error: ' + JSON.stringify(x) +"\n error string: "+ xs + "\n error throwed: " + xt);
            }
        });


    }
}



function cargarPermisos(){
    var rol = $("#cmbRoles").val();
    if (rol > 0){
        $("#mantaLoading").modal('show');
        $.ajax({
            url:'obtenerPermisos',
            data:{'idRol': rol},
            type:'post',
            dataType: "json",
            success: function (response) {
                $("input").each(function(index, value){
                    if(value.id != ''){
                        var check = $("#" +  value.id);
                        if(check.prop('checked')){
                            check.trigger('click');
                        }
                    }        
                });

                $.each(response, function (index, value) { 
                $("#chk" + value.IdPermiso).trigger('click');                  
                });

                $("#mantaLoading").modal('hide');
            },
            statusCode: {
                404: function() {
                alert('El servicio Remextiven no se encuentra disponible');
                }
            },
            error:function(x,xs,xt){
                //nos dara el error si es que hay alguno
                window.open(JSON.stringify(x));
                //alert('error: ' + JSON.stringify(x) +"\n error string: "+ xs + "\n error throwed: " + xt);
            }
        });
    }
}