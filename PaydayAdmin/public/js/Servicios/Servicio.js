$(document).ready(function () {
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#btnAltaServicio').click(AltaServicio);

});



function AbrirModal(){
    $("#txtServicio").val("");
    $("#txtDescripcion").val("");
    $("#mdlNuevoServicio").modal("show");
    $("#divAltaBaja").hide();
    $("#txtIdServicio").val(0);
    $("#chk").prop('checked', true);
}


function AltaServicio(){
    var servicio = $("#txtServicio").val();
    var desc = $("#txtDescripcion").val();
    var idServicio = $("#txtIdServicio").val();
    var activo = 0;
    if ($("#chk").prop('checked')){
        activo = 1;
    }

    if (servicio == "" || servicio == " "){
        alertaToast(tipoAlertaError, "Los campos servicio y descripcion son obligatorios", 3500); 
    }else{
        $("#mantaLoading").modal('show');
        $.ajax({
            url:'ABMServiciosAjax',
            data:{'idServicio': idServicio, 'servicio': servicio, 'descripcion':desc, 'activo': activo},
            type:'post',
            dataType: "json",
            success: function (response) {
                var tabla = $('#serviciosTable').DataTable();
                tabla.clear();
                $.each( response, function( key, value ) {
                tabla.row.add(value);
              });
              tabla.draw(); 
                alertaToast(tipoAlertaOK, "Operacion realizada.", 3500); 
                $("#mantaLoading").modal('hide');    
        },
        statusCode: {              
            404: function() {
            $("#mantaLoading").modal('hide');
            alertaToast(tipoAlertaError, 'El servicio Remextiven no se encuentra disponible', 3500);
            }
        },
        error:function(x,xs,xt){
            $("#mantaLoading").modal('hide');
            console.log(x);
            console.log(xs);
            }
        });
    }
}


function ValidarDatosServicio(){


}


function AbrirEditarServicio(idServicio, servicio, descripcion, activo){
    $("#txtIdServicio").val(idServicio);
    $("#txtServicio").val(servicio);
    $("#txtDescripcion").val(descripcion);
    $("#divAltaBaja").show();
    if (activo == 0 && $('#chk').prop('checked')){
        $('#chk').trigger('click');
    }else if (activo == 1 && !$("#chk").prop('checked')) {
        $('#chk').trigger('click');
    }
    
    $("#mdlNuevoServicio").modal("show");
}

function LimpiarModal(){
    $("#txtIdServicio").val("");
    $("#txtServicio").val("");
    $("#txtDescripcion").val("");
    $("#mdlNuevoServicio").modal("hide");
}



