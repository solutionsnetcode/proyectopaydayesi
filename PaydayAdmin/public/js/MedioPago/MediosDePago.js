$(document).ready(function () {
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#btnAltaMetodoPago').click(AltaMetodoPago);

});



function AbrirModal(){
    $("#txtIdMetodoPago").val(0);
    $("#txtMetodoDePago").val("");
    $("#txtDescripcion").val("");
    $("#txtUrlPago").val("");
    $("#mdlMetodoDePago").modal("show");
    $("#divAltaBaja").hide();
    $("#chk").prop('checked', true);
}


function AltaMetodoPago(){
    var metodo = $("#txtMetodoDePago").val();
    var desc = $("#txtDescripcion").val();
    var url = $("#txtUrlPago").val();
    var idMetodo = $("#txtIdMetodoPago").val();
    var activo = 0;
    if ($("#chk").prop('checked')){
        activo = 1;
    }

    if (metodo == "" || metodo == " " || url == "" || url == " "){
        alertaToast(tipoAlertaError, "Los campos metodo de pago y url son obligatorios", 3500); 
    }else{
        $("#mantaLoading").modal('show');
        $.ajax({
            url:'ABMMetodosDePago',
            data:{'idMetodoPago': idMetodo, 'metodo': metodo, 'descripcion':desc, 'url': url, 'activo': activo},
            type:'post',
            dataType: "json",
            success: function (response) {
                var tabla = $('#metodosTable').DataTable();
                tabla.clear();
                $.each( response, function( key, value ) {
                tabla.row.add(value);
              });
              tabla.draw(); 
              $("·")
                alertaToast(tipoAlertaOK, "Operacion realizada.", 3500); 
                $("#mantaLoading").modal('hide');   
        },
        statusCode: {              
            404: function() {
            $("#mantaLoading").modal('hide');
            alertaToast(tipoAlertaError, 'El servicio Remextiven no se encuentra disponible', 3500);
            }
        },
        error:function(x,xs,xt){
            $("#mantaLoading").modal('hide');
            console.log(x);
            console.log(xs);
            }
        });
    }
}


function ValidarDatosServicio(){


}


function AbrirEditarMetodo(idMetodoPago, metodo, descripcion, url, activo){
    $("#txtIdMetodoPago").val(idMetodoPago);
    $("#txtMetodoDePago").val(metodo);
    $("#txtDescripcion").val(descripcion);
    $("#txtUrlPago").val(url);
    $("#divAltaBaja").show();
    if (activo == 0 && $('#chk').prop('checked')){
        $('#chk').trigger('click');
    }else if (activo == 1 && !$("#chk").prop('checked')) {
        $('#chk').trigger('click');
    }
    
    $("#mdlMetodoDePago").modal("show");
}

function LimpiarModal(){
    $("#txtIdMetodoPago").val("");
    $("#txtMetodoDePago").val("");
    $("#txtDescripcion").val("");
    $("#txtUrlPago").val("");
    $("#mdlNuevoServicio").modal("hide");
}



