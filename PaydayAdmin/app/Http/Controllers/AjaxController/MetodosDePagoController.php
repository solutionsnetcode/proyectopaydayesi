<?php

namespace App\Http\Controllers\AjaxController;

use App\Http\Controllers\Controller;
use App\Modelos\MedioDePago;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MetodosDePagoController extends Controller
{
    protected function ABMMetodoPago(Request $request){
        $datos = $request->all();
        if ($datos['idMetodoPago'] > 0){
            if ($this->ModificarMetodoDePago($datos) != null){
                return json_encode($this->ArrayMetodosPagos());
            }else{
                return response()->json(['respuesta' => 'No se pudo modificar el metodo de pago.'], 500);
            }
        }
        else{
            if ($this->AltaMetodoDePago($datos) != null){
                return json_encode($this->ArrayMetodosPagos());
            }else{
                return response()->json(['respuesta' => 'No se pudo realizar el alta.'], 500);
            }
        }
    }

    protected function ValidarDatosMetodo(array $datos){
        return Validator::make($datos, [
            'metodo' => ['required'],
            'url' => ['required'],
        ])->validate(); 
    }


    protected function AltaMetodoDePago(array $datos){
        return MedioDePago::create([
            'Nombre' => $datos['metodo'],
            'Descripcion' => $datos['descripcion'],
            'FechaRegistro' => Carbon::now(),
            'UrlPago' => $datos['url'],
            'Activo' => 1,
        ]);
    }

    protected function ModificarMetodoDePago(array $datos){
        $metodo = MedioDePago::find($datos['idMetodoPago']);
        if ($metodo != null){
            $metodo->Nombre = $datos['metodo'];
            $metodo->Descripcion = $datos['descripcion'];
            $metodo->UrlPago = $datos['url'];
            $metodo->Activo = $datos['activo'];
            $metodo->save();
        }

        return $metodo;
    }

    protected function ArrayMetodosPagos(){
        $metodosA = array();
        foreach (MedioDePago::all() as $key => $value) {
            $cadenaFuncionJS = "AbrirEditarMetodo(" . $value->IdMedioPago . ", '" . $value->Nombre . "','" . $value->Descripcion . "','" . $value->UrlPago . "'," . $value->Activo . ");";
            
            $nuevoDato = array($value->Nombre, 
                                $value->Descripcion, 
                                $value->UrlPago,
                                ($value->Activo == 1) ? '<span class="text-green">Activo</span>' : '<span class="text-danger">Inactivo</span>',
                                '<a class="text-secondary botonImagen text-center" onclick="' . $cadenaFuncionJS . '"><i class="fas fa-user-edit fa-lg"></i></a>'
                            );
            array_push($metodosA, $nuevoDato);
        }
        return $metodosA;
    }
}
