<?php

namespace App\Http\Controllers\AjaxController;

use App\Http\Controllers\Controller;
use App\Modelos\Servicio;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ServicioController extends Controller
{
    protected function ABMServicio(Request $request){
        $datos = $request->all();
        //$this->ValidarDatosServicio($datos);
        if ($datos['idServicio'] > 0){
            if ($this->ModificarServicio($datos) != null){
                return response(json_encode($this->ArrayServicios()),200);
            }else{
                return response()->json(['respuesta' => 'No se pudo modificar el servicio.'], 500);
            }
        }
        else{
            if ($this->AltaServicio($datos) != null){
                return response(json_encode($this->ArrayServicios()),200);
            }else{
                return response()->json(['respuesta' => 'No se pudo realizar el alta.'], 500);
            }
        }
    }

    protected function ValidarDatosServicio(array $datos){
        return Validator::make($datos, [
            'servicio' => ['required'],
        ])->validate(); 
    }


    protected function AltaServicio(array $datos){
        return Servicio::create([
            'Nombre' => $datos['servicio'],
            'Descripcion' => $datos['descripcion'],
            'FechaRegistro' => Carbon::now(),
            'Activo' => 1,
        ]);
    }

    protected function ModificarServicio(array $datos){
        $servicio = Servicio::find($datos['idServicio']);
        if ($servicio != null){
            $servicio->Nombre = $datos['servicio'];
            $servicio->Descripcion = $datos['descripcion'];
            $servicio->Activo = $datos['activo'];
            $servicio->save();
        }

        return $servicio;
    }

    protected function ArrayServicios(){
        $serviciosA = array();
        foreach (Servicio::all() as $key => $value) {
            $cadenaFuncionJS = "AbrirEditarServicio(" . $value->IdServicio . ", '" . $value->Nombre . "','" . $value->Descripcion . "'," . $value->Activo . ");";
            
            $nuevoDato = array($value->Nombre, 
                                $value->Descripcion, 
                                ($value->Activo == 1) ? '<span class="text-green">Activo</span>' : '<span class="text-danger">Inactivo</span>',
                                '<a class="text-secondary botonImagen text-center" onclick="' . $cadenaFuncionJS . '"><i class="fas fa-user-edit fa-lg"></i></a>'
                            );
            array_push($serviciosA, $nuevoDato);
        }
        return $serviciosA;
    }
}
