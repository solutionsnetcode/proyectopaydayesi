<?php

namespace App\Http\Controllers\AjaxController;

use App\Http\Controllers\Controller;
use App\Modelos\Permiso;
use App\Modelos\PermisosRoles;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PermisosController extends Controller
{
    protected function ObtenerPermisos(Request $request)
    {
        $idRol = $request->input('idRol');
        return PermisosRoles::where('IdRol', $idRol)->get()->toJson();
    }

    protected function ActualizarPermisos(Request $request){
        $datos = $request->all();
        PermisosRoles::where('IdRol', $datos['rol'])->delete();
        $this->InsertarPermisos($datos['permisos'], $datos['rol']);
        return response()->json(['respuesta' => 'Permisos Actulizados'], 200);
    }

    private function InsertarPermisos(array $permisos, $rol){
        foreach ($permisos as $key => $value) {
            DB::table('PermisosDeRoles')->insert([
                'IdRol' => $rol,
                'IdPermiso' => $value,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
    }
}

