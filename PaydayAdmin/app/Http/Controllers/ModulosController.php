<?php

namespace App\Http\Controllers;

use App\Modelos\MedioDePago;
use App\Modelos\Permiso;
use App\Modelos\Rol;
use App\Modelos\Servicio;
use App\User;
use Illuminate\Http\Request;

class ModulosController extends Controller
{
    protected function Dashboard(){
        return view("Dashboard");
    }

    protected function AltaEmpleado(){
        $roles = Rol::where('Activo', 1)->get();
        return view("Usuarios.Empleados.ABMUsuario", compact("roles"));
    }

    protected function ModificarEmpleado($idUsuario){
        $roles = Rol::where('Activo', 1)->get();
        $usuario = User::where('IdUsuario', $idUsuario)->with('Empleado')->first();

        return view("Usuarios.Empleados.ABMUsuario", compact("roles", "usuario"));
    }

    protected function ListadoEmpleados(){
        $empleados = User::where('IdTipoUsuario', 3)->with('Empleado')->get();
        return view("Usuarios.Empleados.ListadoEmpleados", compact("empleados"));
    }

    protected function ListadoClientes(){
        $clientes = User::where('IdTipoUsuario', 1)->with('PersonaFisica')->get();
        return view("Usuarios.Clientes.ListadoClientes", compact("clientes"));
    }

    protected function ListadoServicios(){
        $servicios = Servicio::all();
        return view("Servicios.ListadoServicios", compact("servicios"));
    }

    protected function ListadoMetodosDePago(){
        $metodos = MedioDePago::all();
        return view("MetodosDePago.Listado", compact("metodos"));
    }

    protected function ListadoPermisos(){
        $roles = Rol::where('Activo', 1)->get();
        $permisos = Permiso::orderBy('Permiso')->get();
        return view("Permisos.ListadoPermisos", compact("roles", "permisos"));
    }
    
    
}
