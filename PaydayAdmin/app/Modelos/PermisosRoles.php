<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;

class PermisosRoles extends Model
{
    protected $table = 'PermisosDeRoles';

    protected $fillable = [
        'IdRol', 'IdPermiso'
    ];


    public function Rol(){
        return $this->hasOne(Rol::class, 'IdRol' ,'IdRol');
    }
}
