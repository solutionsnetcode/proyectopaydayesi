<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;

class Servicio extends Model
{
    protected $table = 'Servicios';
    protected $primaryKey = 'IdServicio';

    protected $fillable = [
        'Nombre', 'Descripcion', 'FechaRegistro', 'Activo'
    ];
}
