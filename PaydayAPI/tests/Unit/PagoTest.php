<?php

namespace Tests\Unit;

use App\Modelos\Pago;
use Carbon\Carbon;
use Faker\Factory;
use Tests\TestCase as TestsTestCase;

class PagoTest extends TestsTestCase
{
    /**
     * @test
     */
    public function GenerarPago()
    {
        $faker = Factory::create();

        $nuevoPago = Pago::create([
            'IdUsuario' => random_int(2,3),
            'IdMoneda' => random_int(1,2),
            'MontoPago' =>  random_int(100,2000),
            'IdServicio' => 2,
            'FechaGenera'=> Carbon::now(),   
            'FechaPaga' => Carbon::now(),
            'IdMedioPago' => random_int(1,2),
            'Anulado' => 0
        ]);

        $this->assertDatabaseHas('Pagos', [
            'IdPago' => "$nuevoPago->IdPago",
        ]);
    }

    /**
     * @test
     */
    public function AnularPago()
    {
        $idPago = random_int(11,15);
        $pago = Pago::find($idPago);
        $pago->Anulado = 1;
        $pago->save();


        $this->assertTrue(boolval(Pago::find($idPago)->Anulado));
    }


     /**
     * @test
     */
    public function GenerarPuntosPesos(){
        $pago = new Pago();
        $pago->MontoPago = 500;
        $pago->IdMoneda = 1;

        //Se espera 5 porque 500 *0.01 = 5;
        $this->assertEquals(5,$pago->PuntajeGanado());
    }

    /**
     * @test
     */
    public function GenerarPuntosDolar(){
        $pago = new Pago();
        $pago->MontoPago = 100;
        $pago->IdMoneda = 2;

        //Se espera 45 porque 100 *0.45 = 45;
        $this->assertEquals(45,$pago->PuntajeGanado());
    }
}
