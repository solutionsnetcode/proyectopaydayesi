<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaUsuarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Usuarios', function (Blueprint $table) {
            $table->increments('IdUsuario');
            $table->unsignedTinyInteger('IdTipoUsuario');
            $table->string('Email', 35);
            $table->string('Password',60);
            $table->date('FechaRegistro');
            $table->unsignedTinyInteger('IdRol');
            $table->boolean('Activo');
            $table->rememberToken()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Usuarios');
    }
}
