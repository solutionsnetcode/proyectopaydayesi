<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Usuarios')->insert([
            'IdTipoUsuario' => 1,
            'Email' => 'prueba@hotmail.com',
            'Password' => Hash::make('prueba'),
            'FechaRegistro' => Carbon::today(),
            'IdRol' => 1,
            'Activo' => 1,
           ]);
    }
}
