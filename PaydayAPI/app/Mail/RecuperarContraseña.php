<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RecuperarContraseña extends Mailable
{
    use Queueable, SerializesModels;

    public $tokenActivacion;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($tActivacion)
    {
        $this->tokenActivacion = $tActivacion;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('Cuentas@Payday.com', 'Payday')
       ->subject('Recuperacion de Contraseña')
        ->view('Mails.RecuperarContraseña');
    }
}
