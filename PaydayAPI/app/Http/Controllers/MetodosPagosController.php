<?php

namespace App\Http\Controllers;

use App\Modelos\MedioDePago;
use Illuminate\Http\Request;

class MetodosPagosController extends Controller
{
    public function ListarMetodosDePago(){
        return response(MedioDePago::where('Activo',1)->get()->toJson(),200);
}
}

