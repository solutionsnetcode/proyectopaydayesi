<?php

namespace App\Http\Controllers;

use App\Mail\RecuperarContraseña;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class RecuperarContraseñaController extends Controller
{
    
    public function RecuperarContraseña(Request $request){
        if ($this->ValidarExistenciaUsuario($request->email)){
            $this->EnviarMailRecuperarContraseña($request->email);
            return response()->json(['respuesta' => 'Correo enviado para cambiar la contraseña.'], 200);
        }
        else{
            return response()->json(['respuesta' => 'El usuario no existe.'], 500);
        }
    }

    public function CambioDeContraseña(Request $request){
        $datos = $request->all();

        $usuario = User::find($datos['idUsuario']);
        $usuario->Password = Hash::make($datos['contraseña']);
        $usuario->save();

        return response()->json(['respuesta' => 'Contraseña Actualizada.'], 200);
    }

    public function RecuperarContraseñaView($token){
        if (User::where('TokenActivacion', $token)->first() != null){
            return redirect("http://www.youtube.com.uy");
        }
        else{
            return redirect("http://www.google.com.uy");
        }
    }

    private function ValidarExistenciaUsuario($email){
        $existe = false;
        $usuario = User::where('Email', $email)->first();
        if ($usuario != null){
            $existe = true;
            $usuario->TokenActivacion = Str::random(40);
            $usuario->save();
        }

        return $existe;
    }

    private function EnviarMailRecuperarContraseña(string $email){
        $usuario = User::where('Email', $email)->first();
        Mail::to($usuario->Email)->send(new RecuperarContraseña($usuario->TokenActivacion));
    }


}
