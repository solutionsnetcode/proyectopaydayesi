<?php

namespace App\Http\Controllers;

use App\Modelos\Servicio;
use App\User;
use Illuminate\Http\Request;

class ServiciosController extends Controller
{
    public function ListarServicios($idUsuario = null){
        if ($idUsuario != null){
            $usuario = User::find($idUsuario);
            return response(json_encode($usuario->Servicios),200);
        }else{
            return response(Servicio::where('Activo',1)->get()->toJson(),200);
        }           
    }

    
}
