<?php

namespace App\Http\Controllers;

use App\Mail\RecuperarContraseña;
use App\Modelos\PersonaFisica;
use App\User;
use Carbon\Carbon;
use Facade\FlareClient\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
class UsuarioController extends Controller
{
    public function ObtenerUsuarios($id = null){
        if($id == null){
            return response(User::with('PersonaFisica')->where('Activo',1)->where('IdTipoUsuario', 1)->get()->toJson(), 200);
        }
        else{
            return response(User::with('PersonaFisica')->where('IdUsuario', $id)->where('IdTipoUsuario', 1)->get()->toJson(), 200);
        }
    }

    //Da de alta un nuevo usuario en el sistema
    public function AltaUsuario(Request $request){
        $datosNuevoUsuario = $request->all();   
        $this->ValidarDatosRegistro($datosNuevoUsuario);
        $nuevoUsuario = $this->CrearUsuario($datosNuevoUsuario);
        
        if($nuevoUsuario != null){
            return response(json_encode("Alta de usuario exitosa"), 200);
            //return response()->json(['nuevoUsuario' => $nuevoUsuario, 'respuesta' => 'Alta de usuario exitosa.'], 200);
        }
        else{
            return response(json_encode('Error al generar el alta.'), 500);
            //return response()->json(['nuevoUsuario' => $nuevoUsuario, 'respuesta' => 'Error al generar el alta.'], 500);
        }
    }

    public function ModificacionUsuario(Request $request){
        $datosModificar = $request->all();
        $this->ValidarDatosRegistro($datosModificar);

        $usuarioModificar = User::where('Email',$datosModificar['usuario'])->where('ApiToken', $datosModificar['token'])->first();
            
        if ($usuarioModificar != null){
            if($this->ValidarCorreoModificar($usuarioModificar, $datosModificar['email'])){
                $usuarioModificar->Email = $datosModificar['email'];
                $usuarioModificar->Password = $datosModificar['password'];
                $usuarioModificar->save();
                return response()->json(['respuesta' => 'Usuario modificado correctamente.'], 200);
            }
            else{
                return response()->json(['respuesta' => 'El nuevo correo ya pertenece a otro usuario.'], 500);
            }
        }
            
        else{
            return response()->json(['respuesta' => 'El usuario a modificar no existe.'], 500);
        }

    }

    public function BajaUsuario(Request $request){
        $datos = $request->all();      
        $usuario = User::find($datos['idUsuario']);

        if($usuario != null){
            $usuario->Activo = 0;
            $usuario->save();
            return response()->json(['respuesta' => 'Se realizo la baja del usuario.'], 200);
        }
        else{
            return response()->json(['respuesta' => 'El usuario no existe.'], 200);
        }
    }


    private function ValidarDatosRegistro(array $datos){
        $validacion = Validator::make($datos, [
            'email' => ['required', 'unique:Usuarios,Email','email:rfc,dns'],
            'password' => ['required', 'alpha_num'], 
            'nombre' => ['required'],
            'apellido' => ['required'],
            'documento' => ['required', 'numeric', 'unique:PersonasFisicas,Documento'],
            'sexo' => ['required'],
            'fechaNacimiento' => ['required'],
        ]);

        return response(json_encode($validacion->fails()));
        
    }


    private function CrearUsuario(array $datos){
        $usuario = User::create([
            'IdTipoUsuario' => 1,
            'Email' => $datos['email'],
            'Password' =>  Hash::make($datos['password']),
            'FechaRegistro' => Carbon::today(),
            'Puntos'=> 0,
            'Activo' => 1,
        ]);

        $this->CrearPersonaFisica($usuario->IdUsuario, $datos);

        return $usuario;
    }

    private function CrearPersonaFisica(int $idUsuario, array $datos){
        PersonaFisica::create([
            'IdUsuario' => $idUsuario,
            'Nombre' => $datos['nombre'],
            'Apellido' => $datos['apellido'],
            'Documento' => $datos['documento'],
            'Sexo' => $datos['sexo'],
            'FechaNacimiento'=> $datos['fechaNacimiento'],
        ]);
    }


    private function ValidarCorreoModificar(User $usuario, string $correo){
        if ($correo != ""){
            $user = User::where('Email', $correo)->where('IdUsuarioR', '!=', $usuario->IdUsuarioR)->get();
            if ($user->count() > 0){
                return false; //Mas de un correo igual
            }
            else{
                return true; //No se encontraron otro correo
            }
        }
        else{
            return false;
        }
    }
}
