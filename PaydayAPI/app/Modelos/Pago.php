<?php

namespace App\Modelos;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Pago extends Model
{
    protected $table = 'Pagos';
    protected $primaryKey = 'IdPago';
    protected $porcentajePuntaje = 0.01;

    protected $fillable = [
        'IdUsuario', 'IdMoneda', 'MontoPago', 'IdServicio', 'FechaGenera', 'FechaPaga', 'IdMedioPago', 'Anulado'
    ];



    public function PuntajeGanado(){
        if ($this->IdMoneda != 1){
            $this->porcentajePuntaje = 0.45;
        }

        return $this->MontoPago * $this->porcentajePuntaje;
    }

    public function Usuario(){
        return $this->hasOne(User::class, 'IdUsuario' ,'IdUsuario');
    }

    public function Moneda(){
        return $this->hasOne(Moneda::class, 'IdMoneda' ,'IdMoneda');
    }
}
