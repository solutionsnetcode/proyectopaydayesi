<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;

class Moneda extends Model
{
    protected $table = 'Monedas';
    protected $primaryKey = 'IdMoneda';

    protected $fillable = [
        'Nombre', 'Simbolo', 'Activo'
    ];
}
