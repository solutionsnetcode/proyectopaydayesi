<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;

class ProductoCatalogo extends Model
{
    protected $table = 'ProductosCatalogo';
    protected $primaryKey = 'IdProductoCatalogo';

    protected $fillable = [
        'Nombre', 'Descripcion', 'CostoPuntos', 'Stock', 'Activo'
    ];

}
